const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000

//mongoose connection -to connect to mongoDB
mongoose.connect("mongodb+srv://supeeerb:mongoDB@cluster0.tranx.mongodb.net/task152?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

//We will create notifs if connection to db is success or failed
let db = mongoose.connection;
db.on('error',console.error.bind(console, "connection error."))
db.once('open',()=>console.log("Connected to MongoDB"))

app.use(express.json());

//Routes
//import taskRoutes
const taskRoutes = require('./routes/taskRoutes');
/*
Our server will use a middleware to group all task routes
under /tasks. All endpoints in taskRoutes files will start with /tasks
*/
app.use('/tasks',taskRoutes)

//import userRoutes
const userRoutes = require('./routes/userRoutes');
//group userRoutes under /users
app.use('/users',userRoutes);

app.listen(port,()=>console.log(`Server is running at port ${port}`));