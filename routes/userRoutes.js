const express = require("express");
const router = express.Router();

// const User = require("../models/User");

const userControllers = require("../controllers/userControllers");

router.post('/',userControllers.createUserController);
router.get('/',userControllers.getAllUsersController);
	//Model.find() is a method from our model.
	//This will allow us to find documents 
	//from the collection that the model is connected with.

router.get('/getSingleUser/:id',userControllers.getSingleUserController);

//update a single task's status
//URL: https://localhost:4000/tasks/updateTaskStatus/
router.put('/updateUserStatus/:id',userControllers.updateUserController);

module.exports = router;
