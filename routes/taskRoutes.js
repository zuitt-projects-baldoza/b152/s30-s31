const express = require("express");
/* 
Router() - method from express that allows
us the access to our HTTP method routes.
-will act as a middleware and our routing system.
*/ 
const router = express.Router();

const Task = require('../models/Task');

/* ExpressJS routes should not handle the business logic
of our application. Routes are only meant to route client
request by their endpoint and method. Handling the requests
and responses should not be done in the routes. 

Instead, we should have separate functions to handle our
request and response. These separate functions that handle
requests and responses 
*/

const taskControllers = require('../controllers/taskControllers');
// console.log(taskControllers)

//endpoint: /tasks/
router.post('/', taskControllers.createTaskController);
//get all the task documents and send it to the client:
router.get('/',taskControllers.getAllTasksController);

/* router holds all our routes, it will be 
what we will export or import in another file.
*/

//get a single task's details
router.get('/getSingleTask/:id',taskControllers.getSingleTaskController);

//update a single task's status
//URL: https://localhost:4000/tasks/updateTaskStatus/
router.put('/updateTaskStatus/:id',taskControllers.updateTaskStatusController);

module.exports = router;
