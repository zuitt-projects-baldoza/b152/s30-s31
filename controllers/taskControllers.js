/*
import task model in controllers so our 
controllers or controller functions may have 
access to our Task model.
*/

const Task = require("../models/Task");

/*module.exports add controllers as methods for module,
can be imported in other files. Modules in JS are considered obj*/
module.exports.createTaskController = (req,res) => {
	console.log(req.body);

Task.findOne({name: req.body.name})
.then(result=> {
	// console.log(result)
	if(result !== null && result.name === req.body.name){
		return res.send("Duplicate User Found")
	} else {
		let newTask = new Task ({
		name: req.body.name,
		status: req.body.status
})


	// newTask.save((savedTask,error)=>{
	// 	if(error){
	// 		res.send(error);
	// 	} else {
	// 		res.send(savedTask)
	// 	}

	newTask.save()
	.then(result => res.send(result))
	.catch(error => res.send(error));
			}
		})
		.catch(error => res.send(error));
}
module.exports.getAllTasksController = (req,res)=>{
	//Similar to db.tasks.find({})
	Task.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

module.exports.getSingleTaskController = (req,res) => {
	//res.send("Hello from our getSigleTask route and controller");
	console.log(req.params);
	Task.findById(req.params.id)
	.then(result=> res.send(result))
	.catch(error=> res.send(error));
}

module.exports.updateTaskStatusController = (req,res) => {
	console.log(req.params.id);
	console.log(req.body);

/*third argument: {new:true},
-> return updated version of document we were updating.
By, default, without this argument, findByIdAndUpdate
will return the previous state of the document or prev 
version of the document.
*/

//updates obj will contain field and value we want to update
let updates = {
	status: req.body.status
}

Task.findByIdAndUpdate(req.params.id,updates,{new:true})
.then(updatedUser => res.send(updatedUser))
.catch(error => res.send(error));
}



